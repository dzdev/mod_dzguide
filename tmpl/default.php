<?php
/**
 * @version     1.0.0
 * @package     mod_dzguide
 * @copyright   Copyright (C) 2015. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      DZ Team <dev@dzdev.com> - dzdev.com
 */
 
// no direct access
defined('_JEXEC') or die;
JHtml::script('com_dzguide/vote.js', false, true);
$userId = JFactory::getUser()->get('id');
?>
<div class="dzguide-module<?php echo $moduleclass_sfx; ?>">
    <table class="guide-list" width="100%">
        <?php foreach ($items as $i => $item) : ?>
            <tr>
                <td width="90">
                    <a href="<?= $item->link ?>" title="<?php echo $item->title; ?> by <?php echo JFactory::getUser($item->created_by)->name; ?>"><img src="<?php echo $item->hero_image['dota2']['hphover'];?>" class="img-thumb"/></a>
                </td>
                <td>
                    <h3 class="t1"><a href="<?= $item->link ?>" title="<?php echo $item->title; ?> by <?php echo JFactory::getUser($item->created_by)->name; ?>"><?php echo $item->title; ?></a></h3>
                    
                    <div class="small date"><i class="fa fa-pencil"></i> <?php echo JFactory::getUser($item->created_by)->name; ?> - <i class="fa fa-clock-o"></i> <?php echo JHtml::_('date', $item->modified, JText::_('DZ_DATE_FORMAT')); ?> - <i class="fa fa-shield"></i> <strong><?php echo $item->patch_version;?></strong></div>
                </td>
                <td class="text-center" width="10%">
                    <?php if ($userId) : ?>
                        <a href="#" class="btn btn-success btn-xs btn-vote<?= ($item->uservote === "1") ? ' active' : ''; ?>" data-vote="<?= $item->id; ?>" data-type="guide" data-direction="up">
                            <i class="fa fa-star"><span data-vote-count-of="<?= $item->id; ?>" data-vote-direction="up"><?= $item->upvotes; ?></span></i>
                        </a>
                    <?php else : ?>
                        <span class="btn btn-success btn-xs btn-vote" data-toggle="tooltip" data-placement="bottom" title="<?= JTEXT::_('MOD_DZGUIDE_WARNING_LOG_IN_TO_VOTE'); ?>">
                            <i class="fa fa-star"><?= $item->upvotes; ?></i>
                        </span>
                    <?php endif; ?>
                    <?php if ($userId) : ?>
                        <a href="#" class="btn btn-xs btn-vote<?= ($item->uservote === "0") ? ' active' : ''; ?>" data-vote="<?= $item->id; ?>" data-type="guide" data-direction="down">
                            <i class="fa fa-thumbs-down"><span data-vote-count-of="<?= $item->id; ?>" data-vote-direction="down"><?= $item->downvotes; ?></span></i>
                        </a>
                    <?php else : ?>
                        <span class="btn btn-xs btn-vote" data-toggle="tooltip" data-placement="bottom" title="<?= JTEXT::_('MOD_DZGUIDE_WARNING_LOG_IN_TO_VOTE'); ?>">
                            <i class="fa fa-thumbs-down"><?= $item->downvotes; ?></i>
                        </span>
                    <?php endif; ?>
                </td>
            </tr>
            
        <?php endforeach; ?>
        </table>
</div>
