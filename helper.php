<?php
/**
 * @version     1.0.0
 * @package     mod_dzguide
 * @copyright   Copyright (C) 2015. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      DZ Team <dev@dzdev.com> - dzdev.com
 */
 
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );
JModelLegacy::addIncludePath(JPATH_SITE . '/components/com_dzguide/models/');

abstract class modDZGuideHelper
{
    public static function getList($params) {
        $model = JModelLegacy::getInstance('Guides', 'DZGuideModel', array('ignore_request' => true));
        
        // Set the filters based on the module params
        $model->setState('list.start', 0);
        $model->setState('list.limit', (int) $params->get('display_num', 10));
           
        // Ordering
        $model->setState('list.ordering', $params->get('ordering', 'a.ordering'));
        $model->setState('list.direction', $params->get('direction', 'ASC'));
        
        if ($params->get('display_mode') == 'dynamic') {
            // Get current guide information
            $app = JFactory::getApplication();
            $db = JFactory::getDbo();
            $query = $db->getQuery(true);
            $query->select('hero_id, created_by, GROUP_CONCAT(COALESCE(t.tag_id, -1) SEPARATOR \',\') as tags');
            $query->from('#__dzguide_guides as g');
            $query->where('g.id = ' . $app->input->getInt('id'));
            $query->join('left', '#__contentitem_tag_map as t ON g.id = t.content_item_id AND t.type_alias = \'com_dzguide.guide\'');
            $query->group('g.id');
            $db->setQuery($query);
            $current_guide = $db->loadObject();
            
            if ($current_guide) {
                $dynamic_field = $params->get('dynamic_field', 'created_by');
                if (in_array($dynamic_field, array('hero_id', 'created_by', 'tags'))) {
                    $model->setState('filter.' . $dynamic_field, $current_guide->$dynamic_field);
                }
                $model->setState('filter.exclude_id', $app->input->getInt('id'));
            } else {
                return array();
            }
        }
        
        $items = $model->getItems();
        $menu_id = $params->get('menu_id');
        if ($menu_id) {
            // re-cast all links
            foreach ($items as &$item) {
                $item->link = JRoute::_('index.php?option=com_dzguide&view=guide&id=' . $item->id . ':' . $item->alias . '&Itemid=' . (int) $menu_id);
            }
        }
        
        return $items;
    }
}
