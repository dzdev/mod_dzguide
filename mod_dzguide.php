<?php
/**
 * @version     1.0.0
 * @package     mod_dzguide
 * @copyright   Copyright (C) 2015. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      DZ Team <dev@dzdev.com> - dzdev.com
 */
 
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

// Include the syndicate functions only once
require_once __DIR__ . '/helper.php';

// Dynamic pre-check
$app = JFactory::getApplication();
if ($params->get('display_mode') == 'dynamic' && !(
    $app->input->get('option') == 'com_dzguide' &&
    $app->input->get('view') == 'guide' &&
    $app->input->get('id')
))
    return;

$moduleclass_sfx = htmlspecialchars($params->get('moduleclass_sfx'));
$items = modDZGuideHelper::getList($params);

// Display template
require JModuleHelper::getLayoutPath('mod_dzguide', $params->get('layout', 'default'));
